import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { Drone } from '../models/drone.model';

import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DroneWebService {

  baseUrl = 'https://staj-delivery-services.herokuapp.com/';

  constructor(private http: HttpClient) { }

  getDrones(): Observable<Drone[]> {
    return this.http.get<Drone[]>(this.baseUrl + 'drones')
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  addDrone(drone): Observable<any> {
    return this.http.post(this.baseUrl + 'drones', drone)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  // updateUser(user: User, userId: number): Observable<any> {
  //   return this.http.put(this.baseUrl + 'users/' + userId, user)
  //     .pipe(
  //       catchError((error) => this.handleError(error))
  //     );
  // }

  deleteDrone(droneId: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'drones/' + droneId)
      .pipe(
        catchError((error) => this.handleError(error))
      );
  }

  private handleError(error: HttpErrorResponse) {
    console.log('DroneWebService error', error);

    return throwError('Something bad happened; please try again later.');
  }

}
